<?php

namespace App\Http\Controllers;

use App\ChatConversation;
use Illuminate\Http\Request;

class ChatConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatConversation  $chatConversation
     * @return \Illuminate\Http\Response
     */
    public function show(ChatConversation $chatConversation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatConversation  $chatConversation
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatConversation $chatConversation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatConversation  $chatConversation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatConversation $chatConversation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatConversation  $chatConversation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatConversation $chatConversation)
    {
        //
    }
}
