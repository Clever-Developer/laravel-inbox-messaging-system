<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Message;
use App\Conversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function index(Request $request){
        //
        if($request["conversation_id"] !== null){
            $messages = $this->messages( $request["conversation_id"]);
            return response()->json($messages);
        }
        return response()->json($receiver_id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function messages($conversation_id)
    {
        //
        $messages = \App\Message::where("conversation_id", $conversation_id)->with("receiver", "sender")->get();
        return $messages;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
        $messages = \App\Message::create($request->all());
        $message = \App\Message::where("id", $messages->id)->with("receiver", "sender")->first();
        return response()->json($message, 200);
    }

}
