<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/receiver/get", "Api\UserController@index");
Route::get("/users/get", "Api\ConversationController@index");

Route::get("/messages/get", "Api\MessageController@index");
Route::post("/messages/post", "Api\MessageController@store");


Route::get("/users/work/get", "Api\ConversationChatController@index");

Route::get("/work/get", "Api\ChatController@index");
Route::post("/work/post", "Api\ChatController@store");
Route::post("/arbitrach", "Api\ChatController@arbitrach");
Route::post("/accept", "Api\ChatController@accept");
Route::post("/done", "Api\ChatController@done");
Route::post("/closed", "Api\ChatController@closed");
